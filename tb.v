
module tb();
  reg clk, rst;
  wire [31:0] pcOut;
  wire [31:0]instOut;
  
   A instARM(clk, rst);
           
  initial begin
    clk = 0;
    rst = 0;
     #47
    clk = 1;
    rst = 1;
     #47
    clk = 0;
    rst = 0;
     #47
    clk = 1;  
  end

  always
    #200  clk =  ~clk;   
  
endmodule