
module MEM_Reg(input clk, rst, WB_en_in, MEM_R_en_in,
               input[31:0] ALU_result_in, MEM_read_value_in,
               input[31:0] Dest_in,
               output reg wb_en, MEM_R_en,
               output reg [31:0] ALU_result, Mem_read_value,
               output reg [3:0] Dest);


endmodule
