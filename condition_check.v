module condition_check (input[3:0] sr, input[3:0] cond, output reg out);

//reg out;
  
  always@(*) begin
    case(cond)
      4'b0000: begin 
        if (sr[2] == 1'b1) begin // z set
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b0001: begin 
        if (sr[2] == 1'b0) begin // z clear 
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b0010: begin
        if (sr[1] == 1'b1) begin // c set 
         out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b0011: begin 
       if (sr[1] == 1'b0) begin // c clear 
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b0100: begin
       if (sr[3] == 1'b1) begin // n set 
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b0101: begin
      if (sr[3] == 1'b0) begin // n clear 
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b0110: begin
      if (sr[0] == 1'b1) begin // v set 
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b0111: begin
      if (sr[0] == 1'b0) begin // v clear 
         out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b1000: begin 
      if (sr[1] == 1'b1 && sr[2] == 1'b0) begin // c set and z clear
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b1001: begin
      if (sr[1] == 1'b0 || sr[2] == 1'b1) begin // c clear or z set
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b1010: begin
      if ( sr[3] ==  sr[0] ) begin  // n set and v set or    // n clear and v clear 
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b1011: begin
      if ( sr[3] !=  sr[0] ) begin  
          out = 1'b1;
        end
      else out = 1'b0;
      end
      4'b1100: begin
        if ( sr[2] ==  1'b0 && sr[3] ==  sr[0]  ) begin   //  z== 0 , 
          out = 1'b1;
        end
      else out = 1'b0; 
      end
      4'b1101: begin 
        if ( sr[2] ==  1'b1 || sr[3] !=  sr[0]  ) begin  // z == 1
          out = 1'b1;
        end
      else out = 1'b0; 
      end
      4'b1110: begin
        out = 1'b1;
      end
      4'b1111: begin
        out = 1'b1;
      end
      
      default: begin
        out = 1'b0;
      end
  
  endcase
//  assign out_cond = out;
 end
endmodule