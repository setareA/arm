
module test_ID(input clk, rst, flush, input [31:0] instruction, input[31:0] Result_WB, 
                                                          input [4:0] Dest_wb, input writeBackEn,input hazard,
                                                          input [3:0] SR, input [31:0] pc,
                                                          output reg WB_EN_out, MEM_R_EN_out, MEM_W_EN_out, B_out, S_out,
                                                          output[3:0] EXE_CMD_out,
                                                          output[31:0]PC,
                                                          output[31:0] val_rn_out, val_rm_out,
                                                          output imm_out,
                                                          output[11:0] shift_operand_out,
                                                          output[23:0] signed_imm_24_out,
                                                          output [3:0] dest_out);
wire WB_EN, MEM_R_EN, MEM_W_EN, b, s, imm;
wire[3:0] EXE_CMD;
wire[31:0] val_rn;
wire[31:0] val_rm;
wire[11:0] shift_operand;
wire[23:0] signed_imm_24;
wire [3:0] Dest;
 ID idid(.clk(clk), .rst(rst), .instruction(instruction), .Result_WB(Result_WB), .Dest_wb(Dest_wb),
         .writeBackEn(writeBackEn),.hazard(hazard), .SR(SR),
/*output*/.WB_EN(WB_EN), .MEM_R_EN(MEM_R_EN), .MEM_W_EN(MEM_W_EN), .B(b), .S(s),
         .EXE_CMD(EXE_CMD),.val_rn(val_rn),.val_rm(val_rm), .immediate(imm),
         .shift_operand(shift_operand),.signed_imm_24(signed_imm_24),.Dest(Dest)
         // to hazard detect module
         //output[3:0] src1,src2,
         //output Two_src
         );
         ID_Reg regreg(.clk(clk), .rst(rst),.flush(flush),
              .WB_EN_IN(WB_EN), .MEM_R_EN_IN(MEM_R_EN), .MEM_W_EN_IN(MEM_W_EN),
              .B_IN(b), .S_IN(s),.EXE_CMD_IN(EXE_CMD),
              .PC_in(pc), .val_rn_in(val_rn), .val_rm_in(val_rn), .imm_in(imm),
              .shift_operand_in(shift_operand),.signed_imm_24_in(signed_imm_24), .dest_in(Dest),
          /*output*/   
             .WB_EN_out(WB_EN_out), .MEM_R_EN_out(MEM_R_EN_out), .MEM_W_EN_out(MEM_W_EN_out), .B_out(B_out), .S_out(S_out),
             .EXE_CMD_out(EXE_CMD_out), .PC(PC), .val_rn_out(val_rn_out), .val_rm_out(val_rm_out),
            .imm_out(imm_out), .shift_operand_out(shift_operand_out), .signed_imm_24_out(signed_imm_24_out) ,
            .dest_out(dest_out)
              );
endmodule
