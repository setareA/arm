module A (input clk, rst
  /*         output  [31:0] pcOut, instOut */);
  
  wire [31:0] pc , pc1 ,pc2, pc3, pc4 , pc5 , pc6 , pc7, pc8;
  wire [31:0] instruction;
  
  wire [4:0] Dest_wb;
  wire[31:0] Result_WB;

  reg [31:0] instOutTemp;
  reg [31:0] pcOutTemp;
  
  
  
  IF_Stage instIF (.clk(clk), .rst(rst), .freeze(1'd0), .Branch_taken(1'd0), .BranchAddr(32'd0),
                .PC(pc), .Instruction(instruction));
                
   IF_Stage_Reg instIFReg(.clk(clk), .rst(rst), .freeze(1'd0), .flush(1'd0), .PC_in(pc), .Instruction_in(instruction), 
                      .PC(pc1), .instruction(instOut));
                    
   ID IDStage (.clk(clk), .rst(rst), .PC_in(pc1),.instruction(instOut),.Dest_wb(Dest_wb), .Result_WB(Result_WB), 
               ,.PC(pc2));
   ID_Reg IDStage_Reg(.clk(clk), .rst(rst), .PC_in(pc2),.PC(pc3));
   /*
   MEM MEMStage (.clk(clk), .rst(rst), .PC_in(pc3), .PC(pc4));
   MEM_Reg MEMStage_Reg(.clk(clk), .rst(rst), .PC_in(pc4),.PC(pc5));
   
   EXE EXEStage (.clk(clk), .rst(rst), .PC_in(pc5), .PC(pc6));
   EXE_Reg EXEStage_Reg(.clk(clk), .rst(rst), .PC_in(pc6),.PC(pc7));
   
   WB WBStage (.clk(clk), .rst(rst), .PC_in(pc7), .PC(pc8));
   WB_Reg WBStage_Reg(.clk(clk), .rst(rst), .PC_in(pc8),.PC(pcOut));
   
*/
                      
  // assign pcOut = pcOutTemp;
 //  assign instOut = instOutTemp;
endmodule
