
module EXE(input clk,
          input[3:0] EXE_CMD, 
          input MEM_R_EN, MEM_W_EN, 
          input [31:0]PC,
          input [31:0] val_rn, val_rm,
          input imm,
          input [11:0] shift_operand,
          input[23:0] signed_imm_24,
          input [3:0]sr,
          
          output[31:0] alu_result, br_addr,
          output[3:0] status );


endmodule
