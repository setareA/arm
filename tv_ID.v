module tv_ID();
  reg clk, rst;
  reg [31:0] instruction,pc;
  reg [4:0] Dest_wb; 
  reg writeBackEn;
  reg [3:0] SR;
  
wire WB_EN_out, MEM_R_EN_out, MEM_W_EN_out, B_out, S_out;
wire[3:0] EXE_CMD_out;
wire[31:0]PC,val_rn_out, val_rm_out;
wire imm_out;
wire [11:0] shift_operand_out;
wire [23:0] signed_imm_24_out;
wire[3:0] dest_out;
  
   test_ID tt(.clk(clk), .rst(rst), .flush(0), .instruction(instruction), .Result_WB(32'd21), 
                                                          .Dest_wb(4'd10), .writeBackEn(1'b1), .hazard(1'b0),
                                                          .SR(4'b0000), .pc(32'd24),
                                                          .WB_EN_out(WB_EN_out), .MEM_R_EN_out(MEM_R_EN_out), .MEM_W_EN_out(MEM_W_EN_out), .B_out(B_out), .S_out(S_out),
                                                          .EXE_CMD_out(EXE_CMD_out),
                                                          .PC(PC),
                                                          .val_rn_out(val_rn_out), .val_rm_out(val_rm_out),
                                                          .imm_out(imm_out),
                                                          .shift_operand_out(shift_operand_out),
                                                          .signed_imm_24_out(signed_imm_24_out),
                                                          .dest_out(dest_out));

           
  initial begin
    instruction = 32'b1110_00_1_1101_0_0000_0000_000000010100;
    clk = 0;
    rst = 0;
     #47
    clk = 1;
    rst = 1;
     #47
    clk = 0;
    rst = 0;
     #47
    clk = 1;  
  end

  always
    #200  clk =  ~clk;   
  
endmodule



