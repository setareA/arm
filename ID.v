module ID(input clk, rst,
         // from IF Reg
         input [31:0] instruction,
         // from wb stage
         input[31:0] Result_WB,
         input [4:0] Dest_wb,
         input writeBackEn, // for reg file 
         // from hazard detect mode
         input hazard,
         // from status register
         input[3:0] SR,
         //input[31:0]PC_in,  
         //output[31:0]  PC,
         // to next stage 
         output WB_EN, MEM_R_EN, MEM_W_EN, B, S,
         output[3:0] EXE_CMD,
         output [31:0] val_rn,
         output [31:0] val_rm,
         output immediate,
         output[11:0] shift_operand,
         output[23:0] signed_imm_24,
         output [3:0] Dest
         // to hazard detect module
         //output[3:0] src1,src2,
         //output Two_src
         );
  
  
wire [31:0] regOut1, regOut2;
wire [3:0] outMux;
wire memWriteEn;
wire [3:0] aluCommand;
wire mem_read, mem_write,write_Back_En, bb, sr; 
wire output_cond;
                
Mux #(4)instMuxone(.input1(instruction[3:0]) , .input2(instruction[15:12]),
 .selector(mem_write), .finalOut(outMux));
 

Register_file reg_file(.clk(clk), .rst(rst), .src1(instruction[19:16]), .src2(outMux)
               , .Dest_wb(Dest_wb), .Result_WB(Result_WB), .writeBackEn(writeBackEn),
           /*output*/     .reg1(regOut1), .reg2(regOut2));
               
ControlUnit cu (.mode(instruction[27:26]), .opcode(instruction[24:21]), .s(instruction[20]),
         /*output*/ .aluCommand(aluCommand), .mem_read(mem_read), .mem_write(mem_write), .writeBackEn(write_Back_En),
                  .imm(immediate), .b(bb) , .sr(sr));
                
condition_check cd(.sr(SR), .cond(instruction[31:28]), .out(output_cond));

Mux #(9)instMuxtwo (.input1(9'd0) , .input2({writeBackEn, mem_read, mem_write, aluCommand, bb, sr}), 
                                    .selector((hazard)||(~output_cond)), 
                                    .finalOut({WB_EN, MEM_R_EN, MEM_W_EN, EXE_CMD, B,S}));
                                    
assign val_rn =  regOut1;
assign val_rm =  regOut2;
assign shift_operand = instruction[11:0]; 
assign signed_imm_24 = instruction[23:0];  
assign Dest = instruction[15:12];

endmodule