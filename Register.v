module Register(input clk, rst, input[31:0]in, output [31:0] out);
  reg[31:0] temp=32'd0;
  always@(posedge clk) begin 
    if(rst) begin 
      temp = 0;
    end
  else 
    begin
      temp = in;
    end
  end 
  assign out = temp;
endmodule 


