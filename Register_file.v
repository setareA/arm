module Register_file( input clk, rst, 
                      input[3:0] src1, src2, Dest_wb,
                      input[31:0] Result_WB,
                      input writeBackEn,
                      output[31:0] reg1,reg2);
                      
reg[31:0] registerFile [0:15];
integer i;

      always@(negedge clk) begin
        if (rst) begin
          for (i=0; i<16; i=i+1) registerFile[i] = i;
        end
        else  begin
         if (writeBackEn)
            registerFile[Dest_wb] = Result_WB;
          end
        
      end
      
      assign reg1 = registerFile[src1];
      assign reg2 = registerFile[src2];
                      
endmodule