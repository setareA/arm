module Adder (input[31:0] input1, input[31:0] input2, output [31:0] finalOut);
  assign finalOut = input1 + input2;
endmodule

