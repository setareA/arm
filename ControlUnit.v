module ControlUnit(input[1:0] mode, input[3:0]opcode, input s,
                  output reg [3:0] aluCommand,
                  output reg mem_read, mem_write,
                  output reg writeBackEn,
                  output reg imm,
                  output reg b,
                  output reg sr);
  always @( opcode, mode) begin
    
    aluCommand = 4'd0;
    mem_write = 1'b0;
    mem_read = 1'b0;
    writeBackEn = 1'b0;
    imm = 1'b0;
    b = 1'b0;
    sr = 1'b0;
    
    case({mode,opcode})
      
           6'b001101: begin  // MOV 
            aluCommand = 4'b0001;
            mem_write = 1'b0;
            mem_read = 1'b0;
            writeBackEn = 1'b1;
            imm = 1'b1;
            b = 1'b0;
            sr = s;
           end
           
           6'b001111: begin // MVN
             aluCommand = 4'b1001;
             mem_write = 1'b0;
             mem_read = 1'b0;
             writeBackEn = 1'b1;
             imm = 1'b1;
             b = 1'b0;
             sr = s;
           end
           
           6'b000100: begin  // ADD 
             aluCommand = 4'b0010;
             mem_write = 1'b0;
             mem_read = 1'b0;
             writeBackEn = 1'b1;
             imm = 1'b1;
             b = 1'b0;
             sr = s;
           end
           
           6'b000101: begin // ADC 
             aluCommand = 4'b0011;
             mem_write = 1'b0;
             mem_read = 1'b0;
             writeBackEn = 1'b1;
             imm = 1'b1;
             b = 1'b0;
             sr = s;
           end
           
           6'b000010: begin // SUB
            aluCommand = 4'b0100;
            mem_write = 1'b0;
            mem_read = 1'b0;
            writeBackEn = 1'b1;
            imm = 1'b1;
            b = 1'b0;
            sr = s;
           end 
           
           6'b000110: begin // SBC
            aluCommand = 4'b0101;
            mem_write = 1'b0;
            mem_read = 1'b0; 
            writeBackEn = 1'b1;
            imm = 1'b1;
            b = 1'b0;
            sr = s;
           end
           
           6'b000000: begin // AND
            aluCommand = 4'b0110;
            mem_write = 1'b0;
            mem_read = 1'b0;
            writeBackEn = 1'b1;
            imm = 1'b1;
            b = 1'b0;
            sr = s;
           end
           
           6'b001100: begin // ORR
            aluCommand = 4'b0111;
            mem_write = 1'b0;
            mem_read = 1'b0;
            writeBackEn = 1'b1;
            imm = 1'b1;
            b = 1'b0;
            sr = s;
          end
          
          6'b000001: begin // EOR
            aluCommand = 4'b1000;
            mem_write = 1'b0;
            mem_read = 1'b0;
            writeBackEn = 1'b1;
            imm = 1'b1;
            b = 1'b0;
            sr = s;
          end
          
          6'b001010: begin // CMP
            aluCommand = 4'b0100;
            mem_write = 1'b0;
            mem_read = 1'b0;
            writeBackEn = 1'b0;
            imm = 1'b1;
            b = 1'b0;
            sr = 1'b1;
          end
          
          6'b001000: begin // TST
            aluCommand = 4'b0110;
            mem_write = 1'b0;
            mem_read = 1'b0;
            writeBackEn = 1'b0;
            imm = 1'b1;
            b = 1'b0;
            sr = 1'b1;
          end
          
          6'b010100: begin // LDR , STR
              writeBackEn = 1'b0;
              imm = 1'b0;
              b = 1'b0;
            if (s == 1'd1) begin
                aluCommand = 4'b0010; // LDR
                mem_read = 1'b1;
                mem_write = 1'b0;
                sr = 1'b1;
              end
            else begin
              if (s == 1'd0) begin
                 aluCommand = 4'b0010; // STR
                 mem_write = 1'b1;
                 mem_read = 1'b0;
                 sr = 1'b0;
              end
            end
          end 
           
           default : begin
              aluCommand = 4'd0;
              mem_write = 1'b0;
              mem_read = 1'b0;
              writeBackEn = 1'b0;
              imm = 1'b0;
              b = 1'b0;
              sr = 1'b0;
           end
    endcase
    if ({mode, opcode[3]} == 3'b100) begin
                 mem_write = 1'b0;
                 mem_read = 1'b0;
                 writeBackEn = 1'b0;
                 imm = 1'b1;
                 aluCommand = 4'b0000;
                 b = 1'b1;
    end
  end
endmodule
