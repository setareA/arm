module IF_Stage(input clk, rst, freeze, Branch_taken, input [31:0] BranchAddr,
                output [31:0] PC, Instruction);

  wire [31:0] pcM;
  wire [31:0] pcPlusFour;
  wire [31:0] outMux;
  
   Mux #(32) instMux (.input1(BranchAddr) , .input2(pcPlusFour), .selector(Branch_taken), .finalOut(outMux));
   
    Register pcReg(.clk(clk), .rst(rst), .in(outMux) , .out(pcM));
   
    Adder instAdder(.input1(pcM), .input2(32'd4), .finalOut(pcPlusFour));
   
    InstMem instructionMem(.clk(clk) ,.rst(rst) , .addr(pcM) , .inst(Instruction));
   
   assign PC = pcPlusFour;
  
endmodule