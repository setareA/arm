module register_with_flush #(parameter len = 32) (input clk, rst, flush,
                                                  input [len-1:0] input_val, 
	                                                output reg [len-1:0] output_val);

    always @ (posedge clk)
    begin
        if (rst) output_val = 0;
        else if (flush) output_val = 0;
        else output_val = input_val;
    end 

endmodule 