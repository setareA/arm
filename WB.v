
module WB(input[31:0] ALU_result, MEM_result,
          input MEM_R_en,
          output [31:0] out);

   MUX wb_mux(.input1(MEM_result), .input2(ALU_result),  .selector(MEM_R_en), .finalOut(out));
  
  endmodule
