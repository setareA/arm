
module MEM(input clk, MEMread, MEMwrite,
           input[31:0] address, data,
           output[31:0] MEM_result);
           
 wire[31:0] real_address;
 
 
 assign real_address = (address - 32'd1024) & (32'b11111111_11111111_11111111_11111100);

endmodule
