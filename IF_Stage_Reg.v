module IF_Stage_Reg ( input clk, rst, freeze, flush, input[31:0]PC_in, Instruction_in, 
                      output reg [31:0] PC, instruction);
                   // flush to be implemented 
      always@(posedge clk)begin 
        if (rst) begin
          PC = 32'd0;
          instruction = 32'd0;
        end
      else begin
        if ( flush) begin 
           PC = 32'd0;
          instruction  = 32'd0;
      end 
    else begin
        PC = PC_in;
        instruction  = Instruction_in;
      end
      end
    end  
endmodule
