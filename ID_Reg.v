module ID_Reg(input clk, rst,flush,
              input WB_EN_IN, MEM_R_EN_IN, MEM_W_EN_IN,
              input B_IN, S_IN,
              input[3:0] EXE_CMD_IN,
              input[31:0]PC_in,
              input[31:0] val_rn_in, val_rm_in,
              input imm_in,
              input[11:0] shift_operand_in,
              input[23:0] signed_imm_24_in,
              input [3:0] dest_in,
              
              output reg WB_EN, MEM_R_EN, MEM_W_EN, B, S,
              output[3:0] EXE_CMD,
              output[31:0]PC,
              output[31:0] val_rn, val_rm,
              output imm,
              output[11:0] shift_operand,
              output[23:0] signed_imm_24,
              output [3:0] dest
              );
register_with_flush #(1) reg_WB_EN(.clk(clk), .rst(rst),.flush(flush), .input_val(WB_EN_IN), .output_val(WB_EN));
register_with_flush #(1) reg_MEM_R_EN(.clk(clk), .rst(rst),.flush(flush), .input_val(MEM_R_EN_IN), .output_val(MEM_R_EN));
register_with_flush #(1) reg_MEM_W_EN(.clk(clk), .rst(rst),.flush(flush), .input_val(MEM_W_EN_IN), .output_val(MEM_W_EN));
register_with_flush #(1) reg_B(.clk(clk), .rst(rst),.flush(flush), .input_val(B_IN), .output_val(B));
register_with_flush #(1) reg_S(.clk(clk), .rst(rst),.flush(flush), .input_val(S_IN), .output_val(S));

register_with_flush #(4) reg_EXE_CMD(.clk(clk), .rst(rst),.flush(flush), .input_val(EXE_CMD_IN), .output_val(EXE_CMD));
register_with_flush #(32) reg_PC(.clk(clk), .rst(rst),.flush(flush), .input_val(PC_in), .output_val(PC));
register_with_flush #(32) reg_val_rn(.clk(clk), .rst(rst),.flush(flush), .input_val(val_rn_in), .output_val(val_rn));
register_with_flush #(32) reg_val_rm(.clk(clk), .rst(rst),.flush(flush), .input_val(val_rm_in), .output_val(val_rm));
register_with_flush #(1) reg_imm(.clk(clk), .rst(rst),.flush(flush), .input_val(imm_in), .output_val(imm));

register_with_flush #(32) reg_shift_operand(.clk(clk), .rst(rst),.flush(flush), .input_val(shift_operand_in), .output_val(shift_operand));
register_with_flush #(32) reg_signed_imm_24(.clk(clk), .rst(rst),.flush(flush), .input_val(signed_imm_24_in), .output_val(signed_imm_24));
register_with_flush #(1) reg_dest(.clk(clk), .rst(rst),.flush(flush), .input_val(dest_in), .output_val(dest));


endmodule
