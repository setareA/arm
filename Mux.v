module MUX  #(parameter LEN = 32) ( input [`LEN-1:0] input1, input [`LEN-1:0] input2, input selector,
                                       output [LEN-1:0] finalOut);
	assign finalOut = (selector)? input1: input2;
endmodule 