module InstMem(input clk,rst, input[31:0]addr, output[31:0] inst);
  reg [7:0]  mem [0:23];
  integer i;
  
  always@(*) begin
    if(rst)
      begin 
         for (i=0; i<24; i=i+1) mem[i] = 8'd0;
      end
    else begin 
     { mem[0],mem[1] ,mem[2], mem[3]} = 32'b00000000001000100000000000000000;
     { mem[4],mem[5] ,mem[6], mem[7]} = 32'b00000000011001000000000000000000;
     { mem[8],mem[9] ,mem[10], mem[11]} = 32'b00000000101001100000000000000000;
     { mem[12],mem[13] ,mem[14], mem[15]} = 32'b00000000111010000001000000000000;
     { mem[16],mem[17] ,mem[18], mem[19]} = 32'b00000001001010100001100000000000;
     { mem[20],mem[21] ,mem[22], mem[23]} = 32'b00000001011011000000000000000000;
  //   { mem[24],mem[25] ,mem[26], mem[27]} = 32'b00000001101011100000000000000000;
     
    end
  end
  assign inst = {mem[addr], mem[addr+32'd1],mem[addr+32'd2],mem[addr+32'd3]}; //////
endmodule 
